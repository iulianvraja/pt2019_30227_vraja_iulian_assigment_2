package Gui;

import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Rulare.Myrunn;
import Rulare.Run2;
import structuridate.Coada;



public class Interfata {
	private JFrame j;
private Coada x1,x2,x3,x4;
private static TextArea a1;
private static TextArea a2;
private static TextArea a3;
private static TextArea a4;
private static TextField tt1;
private static TextField tt2;
private static TextField tt3;
private static TextField tt6;
private static TextField nrques;
private static TextField tt4;
private static TextField tt5;

private static TextField tt11;
private static TextField tt22;
private static TextField tt33;
private static TextField tt44;

public Interfata() {
	j=new JFrame();
	j.setVisible(true);
	j.setBounds(40, 40, 600, 600);
	x1=new Coada();
	x2=new Coada();
	x3=new Coada();
	x4=new Coada();
	
	initialize();
	
}

private void initialize() {
	JPanel p=new JPanel();
	p.setVisible(true);
	p.setLayout(null);
	JButton b1=new JButton("Start");
	b1.setBounds(500, 10, 100, 50);
	a1=new TextArea();
	a1.setBounds(10, 60, 300, 50);
	a2=new TextArea();
	a2.setBounds(10, 140, 300, 50);
	a3=new TextArea();
	a3.setBounds(10, 220, 300, 50);
	a4=new TextArea();
	a4.setBounds(10, 300, 300, 50);
	JLabel j1=new JLabel();
	JLabel j3=new JLabel();
	JLabel j4=new JLabel("MinArival");
	JLabel j5=new JLabel("MaxArival");
	JLabel j2=new JLabel();
	JLabel jq=new JLabel("NrofQues");
	tt1=new TextField();
	tt3=new TextField();
	tt4=new TextField();
	tt5=new TextField();
	nrques=new TextField();
	tt22=new TextField();
	tt33=new TextField();
	tt44=new TextField();
	tt6=new TextField();
	tt11=new TextField();
	jq.setBounds(50, 500, 90, 20);
	nrques.setBounds(10, 500, 40,20);
	tt11.setBounds(310, 60, 40, 20);
	tt22.setBounds(310, 140, 40, 20);
	tt33.setBounds(310, 220, 40, 20);
	tt44.setBounds(310, 300, 40, 20);
	
	tt6.setBounds(400,320,40,20);
	tt4.setBounds(400, 240, 40, 20);
	tt5.setBounds(400, 280, 40, 20);
	j4.setBounds(350, 240, 40, 20);
	j5.setBounds(350, 280, 40, 20);
	tt1.setBounds(400, 120, 40, 20);
	j1.setBounds(350, 120, 40, 20);
	j1.setText("Min service time");
	 tt2=new TextField();
	tt2.setBounds(400, 160, 40, 20);
	tt3.setBounds(450,200,40,20);
	j3.setBounds(350, 200, 100, 20);
	j3.setText("Simulation Interval");
	j2.setBounds(350, 160, 40, 20);
	j2.setText("Max service time");
	p.add(tt6);
	p.add(j1);
	p.add(j3);
	p.add(tt3);
	p.add(tt2);
	p.add(tt22);
	p.add(tt11);
	p.add(tt33);
	p.add(tt44);
	p.add(j2);
	p.add(b1);
	p.add(tt1);
	p.add(tt2);
	p.add(a1);
	p.add(a2);
	p.add(a3);
	p.add(a4);
	p.add(tt4);
	p.add(tt5);
	p.add(j4);
	p.add(j5);
	p.add(jq);
	p.add(nrques);
	j.setContentPane(p);
	b1.addActionListener(new ActionListener()  {
		
		
		
		public void actionPerformed(ActionEvent e) {
			 int f=10;
			Myrunn r1=new Myrunn(x1, 3);
		      Myrunn r2=new Myrunn(x2, 3);
		      Myrunn r3=new Myrunn(x3, 3);
		      Myrunn r4=new Myrunn(x4, 3);
		      Run2 r5=new Run2(x1,x2,x3,x4,3);
		      
		      Thread t1=new Thread(r1);
		      Thread t2=new Thread(r2);
		      Thread t3=new Thread(r3);
		      Thread t4=new Thread(r4);
		      Thread t5=new Thread(r5);
		      String s;
		      s=nrques.getText();
		      int nr=Integer.parseInt(s);
		      
		      t5.start();
		      if(nr==1) {
		      t1.start();}
		      if(nr==2) {
			      t1.start();
			      t2.start();}
		      if(nr==3) {
			      t1.start();
			      t2.start();
			      t3.start();}
		      if(nr==4) {
			      t1.start();
			      t2.start();
			      t3.start();
		      t4.start();}
		      
		}});
}
public static void setText(int numberofArea,String s) {
	if(numberofArea==1) {a1.setText(s);}
	if(numberofArea==2) {a2.setText(s);}
	if(numberofArea==3) {a3.setText(s);}
	if(numberofArea==4) {a4.setText(s);}
	
}
public static int getMinservice() {
	return Integer.parseInt(tt1.getText());
}

public static int getMaxservice() {
	return Integer.parseInt(tt2.getText());
}

public static int getSimInterval() {
	return Integer.parseInt(tt3.getText());
}

public static int getMinarival() {
	return Integer.parseInt(tt4.getText());
}

public static int getMaxarival() {
	return Integer.parseInt(tt4.getText());
}
public static void setpeakhour(int x ) {
	String s="";
	s+=x;
	tt6.setText(s);
}
public static void setavg1(float x) {
	String s="";
	s+=x;
	tt11.setText(s);
}
public static void setavg2(float x) {
	String s="";
	s+=x;
	tt22.setText(s);
}
public static void setavg3(float x ) {
	String s="";
	s+=x;
	tt33.setText(s);
}

public static void setavg4(float x ) {
	String s="";
	s+=x;
	tt44.setText(s);
}
}
