package structuridate;

import java.util.ArrayList;

import Clienti.Client;

public class Coada {
private ArrayList<Client> coada=new ArrayList<Client>();
private int sizet;
private int nrcoada;
private static int moment=0;
private static int f=0;
public Coada() {
	sizet=0;
	f++;
	nrcoada=f;
}
public int getNrcoada() {
	return nrcoada;
}

public void add(Client T) {
	coada.add(T);
	sizet++;
	moment++;
	System.out.println("la momentul "+moment+"clientul"+T.getNr()+"cu timpul de asteptare"+T.getTimpasteptare()+" a intrat in coada "+nrcoada);
}
public Client getClient() {
	
	
	return coada.get(0);
	
}
public int timpasteptare() {
	int n=coada.size();
	int s=0;
	int i;
	for(i=0;i<n;i++)
		s+=coada.get(i).getTimpasteptare();
	
	return s;
}
public int delete() {
	
	if(este_goala()==0) {
		moment++;
		System.out.println("la momentul "+moment+"clientul"+coada.get(0).getNr()+"cu timpul de asteptare"+coada.get(0).getTimpasteptare()+" a iesit din coada cu numarul: "+nrcoada);
		coada.remove(0);
		sizet--;
		return 1;
	}
	else 
		return 0;
	
}
public int este_goala() {
	if(sizet==0)
		return 1;
	else
		return 0;
}
public Client get_element(int i) {
	return coada.get(i);
}
public ArrayList<Client> getCoada() {
	return coada;
}

@Override
public String toString() {
	String s="";
	int i;
	int n=coada.size();
	for(i=0;i<n;i++)
	{
		s+="Clientul"+coada.get(i).getNr()+" timpul de asteptare:"+coada.get(i).getTimpasteptare();
	}
	
	return s;
}
public int getSize() {
	return sizet;
}
public void setSize(int size) {
	this.sizet = size;
}
public static int getmoment() {
	return moment;
}

}
